package cs.mad.flashcards.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.MADDatabase
import cs.mad.flashcards.entities.getHardcodedFlashcards


class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val flashcardDao = MADDatabase.getInstance(application).flashcardDao()

        if (flashcardDao.getAll().isEmpty()) {
            flashcardDao.insert(*getHardcodedFlashcards().toTypedArray())
        }

        binding.flashcardList.adapter = FlashcardAdapter(flashcardDao.getAll())

        binding.addFlashcardButton.setOnClickListener {
            val newFlashcard = Flashcard("I'm new!", "Hello!")
            flashcardDao.insert(newFlashcard)
            (binding.flashcardList.adapter as FlashcardAdapter).addItem(newFlashcard)
            binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }

        binding.deleteSetButton.setOnClickListener {
            finish()
        }

        binding.studySetButton.setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }
    }
}
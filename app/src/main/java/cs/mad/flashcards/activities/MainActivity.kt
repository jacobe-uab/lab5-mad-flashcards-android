package cs.mad.flashcards.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.MADDatabase
import cs.mad.flashcards.entities.getHardcodedFlashcardSets

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val flashcardSetDao = MADDatabase.getInstance(application).flashcardSetDao()

        if (flashcardSetDao.getAll().isEmpty()) {
            flashcardSetDao.insert(*getHardcodedFlashcardSets().toTypedArray())
        }

        binding.flashcardSetList.adapter = FlashcardSetAdapter(flashcardSetDao.getAll())

        binding.createSetButton.setOnClickListener {
            val newSet = FlashcardSet("I'm new!")
            flashcardSetDao.insert(newSet)
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(newSet)
            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)
        }
    }
}
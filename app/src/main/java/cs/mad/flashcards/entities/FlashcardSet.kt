package cs.mad.flashcards.entities

import androidx.room.*

@Entity(tableName="flashcard_sets")
data class FlashcardSet(
    val title: String,
    @PrimaryKey(autoGenerate = true) val id: Long? = null
)

fun getHardcodedFlashcardSets(): List<FlashcardSet> {
    return mutableListOf(
        FlashcardSet("Set 1"),
        FlashcardSet( "Set 2"),
        FlashcardSet( "Set 3"),
        FlashcardSet( "Set 4"),
        FlashcardSet( "Set 5"),
        FlashcardSet( "Set 6"),
        FlashcardSet( "Set 7"),
        FlashcardSet( "Set 8"),
        FlashcardSet( "Set 9"),
        FlashcardSet( "Set 10")
    )
}

@Dao
interface FlashcardSetDao {

    @Query("SELECT * FROM flashcard_sets")
    fun getAll(): List<FlashcardSet>

    @Insert
    fun insert(vararg flashcardSet: FlashcardSet)

    @Update
    fun update(flashcardSet: FlashcardSet)

    @Delete
    fun delete(flashcardSet: FlashcardSet)

}
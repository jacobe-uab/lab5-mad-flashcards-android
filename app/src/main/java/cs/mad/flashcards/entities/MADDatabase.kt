package cs.mad.flashcards.entities

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Flashcard::class, FlashcardSet::class], version = 1)
abstract class MADDatabase : RoomDatabase() {
    abstract fun flashcardDao(): FlashcardDao
    abstract fun flashcardSetDao(): FlashcardSetDao

    companion object {
        private var INSTANCE: MADDatabase? = null

        fun getInstance(context: Context): MADDatabase {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: Room.databaseBuilder(
                    context.applicationContext,
                    MADDatabase::class.java, "mad_database"
                )
                .allowMainThreadQueries()
                .build()
                .also { INSTANCE = it }
            }
        }
    }
}

